import argparse
import datetime as dt
import re

import numpy as np
import pandas as pd
import sqlalchemy as sq


def parseNumList(string):
    m = re.match(r'(\d+)(?:-(\d+))?$', string)
    # ^ (or use .split('-'). anyway you like.)
    if not m:
        raise ArgumentTypeError("'" + string + "' is not a range of number. Expected forms like '0-5' or '2'.")
    start = m.group(1)
    end = m.group(2) or start
    return list(range(int(start, 10), int(end, 10) + 1))


parser = argparse.ArgumentParser(description='OpenTTD Activity graphs generator.')
parser.add_argument('-o', '--output', help='Output directory path', required=True)
parser.add_argument('-s', '--server', help='OpenTTD Server Name', required=True)
parser.add_argument('-db', '--database', help='Database connection string (SQLAlchemy)', required=True)
parser.add_argument('--offset', help='Weeks before current one to be processed. 0-current, 1-previos and so on',
                    type=parseNumList, default='0')

args = parser.parse_args()

engine = sq.create_engine(args.database)
today = dt.date.today()
for offset in args.offset:
    monday = today + dt.timedelta(days=-today.weekday(), weeks=(offset * -1))
    sunday = monday + dt.timedelta(days=6)
    filepath = args.output + "/openttd-year_" + args.server + "_week_" + monday.strftime("%d-%m-%y") + "_" + sunday.strftime("%d-%m-%y") + ".csv"
    data_frame = pd.read_sql_query(
        "select distinct on (date_part('year', st.game_date)) "
        "date_part('year', st.game_date), "
        "date_trunc('minute', st.probe_date::timestamp) "
        "from ottd_status as st  "
        "join ottd_source as src "
        "on src.id = st.ottd_source "
        "where name = '%s' "
        "and st.probe_date::date >= '%s' "
        "and st.online is true "
        "order by (date_part('year', st.game_date)), date_trunc('minute', st.probe_date::timestamp);" % (args.server, monday), engine)
    data_frame.to_csv(filepath)
    print(filepath)
