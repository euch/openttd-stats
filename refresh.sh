#!/bin/bash

cd $(dirname `which $0`)

if [ ! -d "venv" ]; then
    pyvenv virtualenv 3.6.0 venv
    venv/bin/pip install -r requirements.txt
fi

target_dir="/var/www/html"

venv/bin/python openttd_report.py -o $target_dir -s $OPENTTD_SERVER_NAME -db $POSTGRES_CONNECTION_OPENTTD_STATS $@
venv/bin/python openttd_week.py -o $target_dir -s $OPENTTD_SERVER_NAME -db $POSTGRES_CONNECTION_OPENTTD_STATS $@