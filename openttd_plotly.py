import argparse
import datetime as dt
import re

import pandas as pd
import sqlalchemy as sq

import plotly.plotly as py
import plotly.graph_objs as go

parser = argparse.ArgumentParser(
    description='OpenTTD Activity graphs generator.')
parser.add_argument(
    '-o', '--output', help='Output directory path', required=True)
parser.add_argument(
    '-s', '--server', help='OpenTTD Server Name', required=True)
parser.add_argument('-db', '--database',
                    help='Database connection string (SQLAlchemy)', required=True)
parser.add_argument('--range', help='Days to be processed. Default=356',
                    default='365')
args = parser.parse_args()
engine = sq.create_engine(args.database)
today = dt.date.today()


def horizon(days):
    return today - dt.timedelta(days=int(args.range))


def file_name(server, days, type):
    return "openttd-" + type + "_" + args.server + "_" + days + "_days"


def file_full(file_name):
    return args.output + "/" + file_name + ".csv"


def save_csv(server, days, type, data_frame):
    f = file_full(file_name(server, days, type))
    print('exported ' + server + ' ' + type +
          ' to ' + f + ' (' + days + ' days)')
    data_frame.to_csv(f)
    return f


def companies_data(server, days):
    return pd.read_sql_query(
        " SELECT date_trunc('minute', probe_date) as probe_date,                "
        " companies as value                                                    "
        " FROM (                                                                "
        "     SELECT probe_date                                                 "
        "           ,companies_online as companies                              "
        "           ,lag(companies_online) OVER w as prev_comp_online           "
        "     FROM   ottd_status as st                                          "
        "     join ottd_source as src on src.id = st.ottd_source                "
        "     where probe_date::date >= '%s' and online is true                 "
        "     and name = '%s'                                                   "
        "         WINDOW w AS ( ORDER BY st.probe_date)                         "
        "     ) tokcat                                                          "
        " WHERE  companies <> prev_comp_online                                  "
        % (horizon(days), server), engine)


def players_data(server, days):
    return pd.read_sql_query(
        " SELECT date_trunc('minute', probe_date) as probe_date,                "
        " players_online as value                                               "
        " FROM (                                                                "
        "     SELECT probe_date                                                 "
        "           ,players_online                                             "
        "           ,lag(players_online) OVER w as prev_players_online          "
        "       FROM   ottd_status as st                                        "
        "     join ottd_source as src on src.id = st.ottd_source                "
        "     where probe_date::date >= '%s' and online is true                 "
        "   and name = '%s'                                                     "
        "         WINDOW w AS ( ORDER BY st.probe_date)                         "
        "     ) tokcat                                                          "
        " WHERE  prev_players_online <> players_online                          "
        % (horizon(days), server), engine)


def years_data(server, days):
    return pd.read_sql_query(
        " SELECT date_trunc('minute', probe_date) as probe_date,                "
        " game_year::numeric::integer as value                                  "
        " FROM (                                                                "
        "     SELECT probe_date                                                 "
        "           ,date_part('year', game_date) as game_year                  "
        "           ,lag(date_part('year', game_date)) OVER w as prev_game_year "
        "     FROM   ottd_status as st                                          "
        "     join ottd_source as src on src.id = st.ottd_source                "
        "     where probe_date::date >= '%s' and online is true                 "
        "     and name = '%s'                                                   "
        "         WINDOW w AS ( ORDER BY st.probe_date)                         "
        "     ) tokcat                                                          "
        " WHERE  game_year <> prev_game_year                                    "
        % (horizon(days), server), engine)


def draw(server, type, fp):
    print("drawing from file " + fp)
    df = pd.read_csv(fp)

    trace = go.Scatter(
        x=df['probe_date'],
        y=df['value'],
        name=type,
        line=dict(color='#7F7F7F'),
        opacity=0.8)

    data = [trace]

    layout = dict(
        title='OpenTTD (' + server + ') ' + type,
        xaxis=dict(
            rangeselector=dict(
                buttons=list([
                    dict(count=1,
                         label='1m',
                         step='month',
                         stepmode='backward'),
                    dict(count=6,
                         label='6m',
                         step='month',
                         stepmode='backward'),
                    dict(step='all')
                ])
            ),
            rangeslider=dict(),
            type='date'
        )
    )

    fig = dict(data=data, layout=layout)
    py.plot(fig, filename=type)
    print("OK")


def any_data(server, days, type):
    if (type == 'players'):
        return players_data(server, days)
    elif (type == 'companies'):
        return companies_data(server, days)
    elif (type == 'years'):
        return years_data(server, days)


def save_n_draw(server, days, type):
    data_frame = any_data(server, days, type)
    file_full = save_csv(server, days, type, data_frame)
    draw(server, type, file_full)

save_n_draw(args.server, args.range, 'players')
save_n_draw(args.server, args.range, 'companies')
save_n_draw(args.server, args.range, 'years')
